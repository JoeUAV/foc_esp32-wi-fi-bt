#ifndef __PIN_CONFIG__
#define __PIN_CONFIG__
/****Power Check****/
#define POWER_VOL_PIN 34
/****FOC drivers & encoders****/
#define FOC_CH1_SDA_PIN 15
#define FOC_CH1_SCL_PIN 13
#define FOC_CH2_SDA_PIN 9
#define FOC_CH2_SCL_PIN 10

#define FOC_CH1_A 33 
#define FOC_CH1_B 25
#define FOC_CH1_Z 26

#define FOC_CH1_CH1 0 
#define FOC_CH1_CH2 1
#define FOC_CH1_CH3 2

#define FOC_CH1_EN 27

#define FOC_CH2_A 19
#define FOC_CH2_B 22
#define FOC_CH2_Z 21

#define FOC_CH2_CH1 3 
#define FOC_CH2_CH2 4
#define FOC_CH2_CH3 5

#define FOC_CH2_EN 23

/****Button & Led****/
#define BUTTON_PIN 4
#define LED_PIN 5

/****UART****/
#define UART_RX 32
#define UART_TX 18

#endif