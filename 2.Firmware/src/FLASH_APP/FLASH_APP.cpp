#include "FLASH_APP/FLASH_APP.h"

void FLASH::read(void){
Serial.println("Start read");

  EEPROM.begin(4096); //申请操作到地址4095（比如你只需要读写地址为100上的一个字节，该处也需输入参数101）
  for(int addr = 0; addr<4096; addr++)
  {
    int data = EEPROM.read(addr); //读数据
    Serial.print(data);
    Serial.print(" ");
    delay(2);
    if((addr+1)%256 == 0) //每读取256字节数据换行
    {
      Serial.println("");
    }
  }

  Serial.println("End read");
}